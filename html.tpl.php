<!DOCTYPE html>
<html lang="en">

<head profile="<?php print $grddl_profile; ?>">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
   <!-- Custom Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php print $scripts; ?>
</head>

<body id="page-top" class="index">
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>

   <!-- jQuery 
    <script src="js/jquery.js"></script>-->

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo drupal_get_path('theme', 've_drupal');?>/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo drupal_get_path('theme', 've_drupal');?>/js/jquery.easing.min.js"></script>
    <script src="<?php echo drupal_get_path('theme', 've_drupal');?>/js/classie.js"></script>
    <script src="<?php echo drupal_get_path('theme', 've_drupal');?>/js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
<!--    <script src="<?php echo drupal_get_path('theme', 've_drupal');?>/js/jqBootstrapValidation.js"></script>
    <script src="<?php echo drupal_get_path('theme', 've_drupal');?>/js/contact_me.js"></script> -->

    <!-- Custom Theme JavaScript
    <script src="<?php echo drupal_get_path('theme', 've_drupal');?>/js/agency.js"></script> -->

</body>
</html>