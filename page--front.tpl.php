
	<!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<?php if ($logo): ?>
				  <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				  </a>
				  <?php endif; ?>

			<!--- Site Name --->
			 <?php if (!empty($site_name)): ?>
				  <a class="navbar-brand page-scroll" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
			  <?php endif; ?>
                
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            
			 <?php if ($main_menu): ?>
      <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
        <?php print theme('links__system_main_menu', array(
          'links' => $main_menu,
          'attributes' => array(
            'id' => 'main-menu-links',
            'class' => array('nav', 'navbar-nav','navbar-right'),
          ),
          'heading' => array(
            'text' => t('Main menu'),
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        )); ?>
      </div> <!-- /#main-menu -->
    <?php endif; ?>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Welcome To Our Drupal Theme!</div>
                <div class="intro-heading">It's Nice To Meet You</div>
                <a href="#services" class="page-scroll btn btn-xl">Tell Me More</a>
            </div>
        </div>
    </header>

    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Services</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row text-center">
				<?php if($page['page_top1']):?>
					<div class="col-md-4">
						<?php print render($page['page_top1']); ?>
					</div>
				<?php endif; ?>
				<?php if($page['page_top2']):?>
					<div class="col-md-4">
						<?php print render($page['page_top2']); ?>
					</div>
				<?php endif; ?>
				<?php if($page['page_top3']):?>
					<div class="col-md-4">
						<?php print render($page['page_top3']); ?>
					</div>
				<?php endif; ?>
            </div>
        </div>
    </section>

   

    <!-- Home Section -->
    <section id="homepage" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                     <?php print render($title_prefix); ?>
					  <?php if ($title): ?>
						<h2 class="section-heading" id="page-title">
						  <?php print $title; ?>
						</h2>
					  <?php endif; ?>
					  <?php print render($title_suffix); ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
				<?php print render($title_suffix); ?>
				  <?php if ($tabs): ?>
					<div class="tabs">
					  <?php print render($tabs); ?>
					</div>
				  <?php endif; ?>
                    <?php print render($page['help']); ?>
					  <?php if ($action_links): ?>
						<ul class="action-links">
						  <?php print render($action_links); ?>
						</ul>
					  <?php endif; ?>
					<?php print render($page['content']); ?>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
			<div class="row text-left">
				<?php if($page['page_bottom1']): ?>
                <div class="col-md-4">
					<?php print render($page['page_bottom1']); ?>	
                </div>
				<?php endif; ?>
				<?php if($page['page_bottom2']): ?>
                <div class="col-md-4">
                  <?php print render($page['page_bottom2']); ?>
                </div>
				<?php endif; ?>
				<?php if($page['page_bottom3']): ?>
                <div class="col-md-4">
                   <?php print render($page['page_bottom3']); ?>
                </div>
				<?php endif; ?>
            </div>
			<div class="row"><hr></div>
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Virtual Employee Pvt. Ltd. 2015</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons no-blt">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks no-blt">
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    