
	<span class="fa-stack fa-4x">
		<i class="fa fa-circle fa-stack-2x text-primary"></i>
		<i class="fa fa-lock fa-stack-1x fa-inverse <?php print $block_html_id; ?>"></i>
	</span>
	<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

	  <?php print render($title_prefix); ?>
	<?php if ($block->subject): ?>
	  <h4 class="service-heading" <?php print $title_attributes; ?>><?php print $block->subject ?></h4>
	<?php endif;?>
	  <?php print render($title_suffix); ?>

	  <div class="text-muted"<?php print $content_attributes; ?>>
		<?php print $content ?>
	  </div>
	</div>
